$(document).ready(function () {
  var count = 0;
  $('.posts__show-more').on('click', function () {
    for (var i = 1 + count; i < 7 + count; i++) {
      var promise = getPost(i);
      promise.then(addingItems);
    }
    count = count + 6;
  });
  function addingItems(data) {
    console.log(data.length);

    if (data.length > 0) {
      data.forEach(function (item, i, arr) {
        var title = item.title;
        title = title[0].toUpperCase() + title.substr(1).toLowerCase();
        var body = item.body;
        body = body[0].toUpperCase() + body.substr(1).toLowerCase();
        var elem = "\n      <a href=\"#\" class=\"col-flex posts__item\">\n " +
          "<div class=\"posts__title\">\n ".concat(title, "\n </div>\n " +
            "<div class=\"posts__body\">\n ").concat(body, "\n </div>\n " +
            "<div class=\"posts__further\">\n Read More\n </div>\n </a>\n ");
        $('.row-post').append(elem);
      });
    } else {
      $('.posts__show-more').attr('disabled', 'disabled').addClass('disabled');
    }
  }
})